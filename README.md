Areas with potential for managing flood risk by protecting, restoring and emulating the natural regulating function of catchments and rivers. 

The areas are classified according to the features defined in Section 2 of the guide published here:
 
https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/725264/Working_with_natural_processes_mapping_user_guide.pdf
 
The areas of the features described in that report have been computed per Water Framework Directive cycle 2 Waterbody catchment polygons.

Input data sets are:

WFD River Waterbody Catchments Cycle 2 (Open Government licence)
WWNP layers (all are supplied under open government licence) for:
Floodplain Reconnection Potential
Riparian Woodland Potential
Floodplain Woodland Potential
Wider Catchment Woodland Potential
Runoff Attenuation Features 1% AEP
Runoff Attenuation Features 3.3% AEP
Woodland data constrained by woodland constraints
Predictive Agricultural Land Classification (ALC) 

All input data sets are available from data.gov.uk under the Open Government License http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/

